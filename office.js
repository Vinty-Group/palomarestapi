// Создаем HTTP-запрос
var xhr = new XMLHttpRequest();
xhr.open('GET', 'http://localhost:8080/exell/officeBook');

// Обработчик ответа от сервера
xhr.onload = function() {
    if (xhr.status === 200) {
        let data = JSON.parse(xhr.responseText);
        let tableBody = document.getElementById('table-body');
        // Проходим по всем записям в JSON и добавляем их в таблицу
        for (var key in data) {
            let row = document.createElement('tr');
            let jobTitleCell = document.createElement('td');
            let responsibleForCell = document.createElement('td');
            let fullNameCell = document.createElement('td');
            let phoneCell = document.createElement('td');
            jobTitleCell.innerText = data[key].officeCells.jobTitle;
            responsibleForCell.innerText = data[key].officeCells.responsibleFor;
            fullNameCell.innerText = data[key].officeCells.fullName;
            phoneCell.innerText = data[key].officeCells.phone;

            row.appendChild(jobTitleCell);
            row.appendChild(responsibleForCell);
            row.appendChild(fullNameCell);
            row.appendChild(phoneCell);
            tableBody.appendChild(row);
            if (data[key].typeRecord === "$$header") {
                row.classList.add('orange-background');
            }
            if (data[key].typeRecord === "$$subHeader") {
                row.classList.add('orange-light-background');
            }
        }
    } else {
        console.log('Ошибка загрузки данных');
    }
};

// Отправляем запрос на сервер
xhr.send();
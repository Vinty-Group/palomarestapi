package by.zyskunov.RestApiPrc.services;

import by.zyskunov.RestApiPrc.models.OfficeCell.*;
import by.zyskunov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
@RequiredArgsConstructor
public class AngularGetOfficeService {
    private final OfficeCellServiceImpl officeCellService;

    public Map<String, AngularSaveOfficeCellDTO> GetOfficeData() {
        Map<String, AngularSaveOfficeCellDTO> responseDataMap = new HashMap<>();

        ResponseList<PostgresGetOfficeCellDTO> bdResponse = officeCellService.getOfficeCell();
        List<PostgresGetOfficeCellDTO> bdList = bdResponse.getContent();

        for (PostgresGetOfficeCellDTO cellDTO : bdList) {

            responseDataMap.put(cellDTO.getMainKey(), GetRespData(cellDTO.getId().toString(),
                    cellDTO.getCreateDate(),
                    cellDTO.getFullName(),
                    cellDTO.getJobTitle(),
                    cellDTO.getPhone(),
                    cellDTO.getResponsibleFor(),
                    cellDTO.getTitleOrder(),
                    cellDTO.getParentKey(),
                    cellDTO.getTypeRecord(),
                    cellDTO.getFiredStatus()
            ));
        }

        return responseDataMap;
    }

    public AngularSaveOfficeCellDTO GetRespData(String id, String date, String fullName, String jobTitle, String phone, String responsibleFor, Long order, String parent, String typeRecord, Long firedStatus) {
        AngularSaveOfficeCellDTO responseData = new AngularSaveOfficeCellDTO();
        responseData.setId(id); // парент айди
        responseData.setDate(date);

        AngularOfficeCellDTO angularCellDTO = new AngularOfficeCellDTO();
        angularCellDTO.setFullName(fullName); //имя
        angularCellDTO.setJobTitle(jobTitle);
        angularCellDTO.setPhone(phone);
        angularCellDTO.setResponsibleFor(responsibleFor);

        responseData.setOrder(order);
        responseData.setFiredStatus(firedStatus);
        responseData.setParent(parent);
        responseData.setTypeRecord(typeRecord);

        responseData.setOfficeCells(angularCellDTO);

        return responseData;
    }
}

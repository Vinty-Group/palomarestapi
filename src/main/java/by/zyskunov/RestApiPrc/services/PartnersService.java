package by.zyskunov.RestApiPrc.services;

import by.zyskunov.RestApiPrc.dao.DeletePartnerDao;
import by.zyskunov.RestApiPrc.dao.SaveOfficeCellDao;
import by.zyskunov.RestApiPrc.dao.UpdateOfficeCellDao;
import by.zyskunov.RestApiPrc.models.PartnersCell.*;
import by.zyskunov.RestApiPrc.utils.JsonHelper;
import by.zyskunov.RestApiPrc.utils.ResponseList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.ls.LSOutput;

import java.util.*;

@Log4j2
@Service
@RequiredArgsConstructor

public class PartnersService {
    private final OfficeCellServiceImpl officeCellService;
    private final DeletePartnerDao deletePartnerDao;
    private final JsonHelper jsonHelper;
    private final SaveOfficeCellDao saveOfficeCellDao;
    private final UpdateOfficeCellDao updateOfficeCellDao;

    public Map<String, AngularSavePartnersCellDTO> getPartnerData() {
        Map<String, AngularSavePartnersCellDTO> responseDataMap = new HashMap<>();

        ResponseList<PostgresGetPartnersCellDTO> bdResponse = officeCellService.getPartnersCell();
        List<PostgresGetPartnersCellDTO> bdList = bdResponse.getContent(); // лист с данными из бд

        for (PostgresGetPartnersCellDTO cellDTO : bdList) {

            AngularSavePartnersCellDTO responseData = getRespPartnerData(
                    cellDTO.getId(),
                    cellDTO.getCreateDate(),
                    cellDTO.getTitleOrder(),
                    cellDTO.getServiceName(),
                    cellDTO.getBankName(),
                    cellDTO.getFullName(),
                    cellDTO.getPhone(),
                    cellDTO.getDescription()
            );

            responseDataMap.put(String.valueOf(cellDTO.getId()), responseData);

        }

        return responseDataMap;
    }

    // вернуть обьект для отправики в ангулар трейдерсы в том виде что нужно ангулару
    private AngularSavePartnersCellDTO getRespPartnerData(Long id, String date, Long order, String serviceName, String bankName, String fullName, String phone, String description) {

        AngularSavePartnersCellDTO responseData = new AngularSavePartnersCellDTO();

        responseData.setId(id);
        responseData.setDate(date);
        responseData.setOrder(order);

        AngularPartnersCellDTO angularPartnersCellDTO = new AngularPartnersCellDTO();
        angularPartnersCellDTO.setServiceName(serviceName);
        angularPartnersCellDTO.setBankName(bankName);
        angularPartnersCellDTO.setFullName(fullName);
        angularPartnersCellDTO.setPhone(phone);
        angularPartnersCellDTO.setDescription(description);

        responseData.setPartners(angularPartnersCellDTO);

        return responseData;
    }

    @Transactional
    public String deletePartnersCell(int id) {
        return deletePartnerDao.deletePartnerCell(id);
    }

    public void savePartnersCell(String json)
    {
        AngularSavePartnersCellDTO angularSavePartnersCellDTO = new AngularSavePartnersCellDTO();
        PostgresSavePartnersCellDTO postgresSavePartnersCellDTO = new PostgresSavePartnersCellDTO();

        if (Objects.nonNull(json) && jsonHelper.isValid(json))
        {
            ObjectMapper objectMapper = new ObjectMapper();

            try
            {
                angularSavePartnersCellDTO = objectMapper.readValue(json, AngularSavePartnersCellDTO.class);

            } catch (JsonProcessingException e)
            {
                throw new RuntimeException(e);
            }
        }

//        String randomId = generateRandomSevenDigitNumber();

        postgresSavePartnersCellDTO.setCreateDate(angularSavePartnersCellDTO.getDate());
        postgresSavePartnersCellDTO.setTitleOrder(angularSavePartnersCellDTO.getOrder());

        postgresSavePartnersCellDTO.setServiceName(angularSavePartnersCellDTO.getPartners().getServiceName());
        postgresSavePartnersCellDTO.setBankName(angularSavePartnersCellDTO.getPartners().getBankName());
        postgresSavePartnersCellDTO.setFullName(angularSavePartnersCellDTO.getPartners().getFullName());
        postgresSavePartnersCellDTO.setPhone(angularSavePartnersCellDTO.getPartners().getPhone());
        postgresSavePartnersCellDTO.setDescription(angularSavePartnersCellDTO.getPartners().getDescription());

        saveOfficeCellDao.savePartnerCell(PostgresSavePartnersCellDTO.builder()
                .createDate(postgresSavePartnersCellDTO.getCreateDate())
                .titleOrder(postgresSavePartnersCellDTO.getTitleOrder())

                .serviceName(postgresSavePartnersCellDTO.getServiceName())
                .bankName(postgresSavePartnersCellDTO.getBankName())
                .fullName(postgresSavePartnersCellDTO.getFullName())
                .phone(postgresSavePartnersCellDTO.getPhone())
                .description(postgresSavePartnersCellDTO.getDescription())
                .build());
    }

    @Transactional
    public void updatePartnersCell(String json)
    {
        AngularSavePartnersCellDTO angularSavePartnersCellDTO = new AngularSavePartnersCellDTO();
        PostgresUpdatePartnersCellDTO postgresUpdatePartnersCellDTO = new PostgresUpdatePartnersCellDTO();

        if (Objects.nonNull(json) && jsonHelper.isValid(json))
        {
            ObjectMapper objectMapper = new ObjectMapper();

            try
            {
                angularSavePartnersCellDTO = objectMapper.readValue(json, AngularSavePartnersCellDTO.class);

            } catch (JsonProcessingException e)
            {
                throw new RuntimeException(e);
            }
        }

//        String randomId = generateRandomSevenDigitNumber();

        postgresUpdatePartnersCellDTO.setId(angularSavePartnersCellDTO.getId());
//        postgresUpdatePartnersCellDTO.setMainKey(angularSavePartnersCellDTO.);
        postgresUpdatePartnersCellDTO.setCreateDate(angularSavePartnersCellDTO.getDate());
        postgresUpdatePartnersCellDTO.setTitleOrder(angularSavePartnersCellDTO.getOrder());

        postgresUpdatePartnersCellDTO.setServiceName(angularSavePartnersCellDTO.getPartners().getServiceName());
        postgresUpdatePartnersCellDTO.setBankName(angularSavePartnersCellDTO.getPartners().getBankName());
        postgresUpdatePartnersCellDTO.setFullName(angularSavePartnersCellDTO.getPartners().getFullName());
        postgresUpdatePartnersCellDTO.setPhone(angularSavePartnersCellDTO.getPartners().getPhone());
        postgresUpdatePartnersCellDTO.setDescription(angularSavePartnersCellDTO.getPartners().getDescription());

        updateOfficeCellDao.updatePartnersCell(PostgresUpdatePartnersCellDTO.builder()
                .id(postgresUpdatePartnersCellDTO.getId())
                .createDate(postgresUpdatePartnersCellDTO.getCreateDate())
                .titleOrder(postgresUpdatePartnersCellDTO.getTitleOrder())

                .serviceName(postgresUpdatePartnersCellDTO.getServiceName())
                .bankName(postgresUpdatePartnersCellDTO.getBankName())
                .fullName(postgresUpdatePartnersCellDTO.getFullName())
                .phone(postgresUpdatePartnersCellDTO.getPhone())
                .description(postgresUpdatePartnersCellDTO.getDescription())
                .build());
    }

    public String generateRandomSevenDigitNumber() {
        Random random = new Random();
        int min = 1000000; // Минимальное 7-значное число
        int max = 9999999; // Максимальное 7-значное число
        int number = random.nextInt(max - min + 1) + min;
        return String.valueOf(number); // Преобразуем число в строку
    }
}

package by.zyskunov.RestApiPrc.services;

import by.zyskunov.RestApiPrc.dao.DeleteTraderDao;
import by.zyskunov.RestApiPrc.dao.SaveOfficeCellDao;
import by.zyskunov.RestApiPrc.dao.UpdateOfficeCellDao;
import by.zyskunov.RestApiPrc.models.Asterix.AsterixDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.*;
import by.zyskunov.RestApiPrc.utils.JsonHelper;
import by.zyskunov.RestApiPrc.utils.ResponseList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor

public class TraderService {
    private final OfficeCellServiceImpl officeCellService;
    private final DeleteTraderDao deleteTraderDao;
    private final JsonHelper jsonHelper;
    private final SaveOfficeCellDao saveOfficeCellDao;
    private final UpdateOfficeCellDao updateOfficeCellDao;

    public Map<String, AngularSaveTraderCellDTO> GetTradersData() {
        Map<String, AngularSaveTraderCellDTO> responseDataMap = new HashMap<>();

        ResponseList<PostgresGetTraderCellDTO> bdResponse = officeCellService.getTraderCell();
        List<PostgresGetTraderCellDTO> bdList = bdResponse.getContent(); // лист с данными из бд

        for (PostgresGetTraderCellDTO cellDTO : bdList) {

            AngularSaveTraderCellDTO responseData = GetRespTraderData(
                    cellDTO.getId(),
                    cellDTO.getCreateDate(),
                    cellDTO.getTitleOrder(),
                    cellDTO.getCodeTo(),
                    cellDTO.getCity(),
                    cellDTO.getAddress(),
                    cellDTO.getPlace(),
                    cellDTO.getMobileOperator(),
                    cellDTO.getSchedule(),
                    cellDTO.getPhone(),
                    cellDTO.getHeadOfSector()
            );

            responseDataMap.put(cellDTO.getMainKey(), responseData);

        }

        return responseDataMap;
    }

    public Map<String, AngularSaveTraderCellDTO> GetPortativData() {
        Map<String, AngularSaveTraderCellDTO> responseDataMap = new HashMap<>();

        ResponseList<PostgresGetTraderCellDTO> bdResponse = officeCellService.getPortativCell();
        List<PostgresGetTraderCellDTO> bdList = bdResponse.getContent(); // лист с данными из бд

        for (PostgresGetTraderCellDTO cellDTO : bdList) {

            AngularSaveTraderCellDTO responseData = GetRespTraderData(
                    cellDTO.getId(),
                    cellDTO.getCreateDate(),
                    cellDTO.getTitleOrder(),
                    cellDTO.getCodeTo(),
                    cellDTO.getCity(),
                    cellDTO.getAddress(),
                    cellDTO.getPlace(),
                    cellDTO.getMobileOperator(),
                    cellDTO.getSchedule(),
                    cellDTO.getPhone(),
                    cellDTO.getHeadOfSector()
            );

            responseDataMap.put(cellDTO.getMainKey(), responseData);

        }

        return responseDataMap;
    }

    // вернуть обьект для отправики в ангулар трейдерсы в том виде что нужно ангулару
    private AngularSaveTraderCellDTO GetRespTraderData(Long id, String date, Long order, String codeTO, String city, String address, String place, String operator, String schedule, String phone, String headOfSector) {

        AngularSaveTraderCellDTO responseData = new AngularSaveTraderCellDTO();

        responseData.setId(String.valueOf(id));
        responseData.setDate(date);
        responseData.setOrder(order);

        AngularTraderCellDTO angularTraderCellDTO = new AngularTraderCellDTO();
        angularTraderCellDTO.setCodeTO(codeTO);
        angularTraderCellDTO.setCity(city);
        angularTraderCellDTO.setAddress(address);
        angularTraderCellDTO.setPlace(place);
        angularTraderCellDTO.setOperator(operator);
        angularTraderCellDTO.setSchedule(schedule);
        angularTraderCellDTO.setPhone(phone);
        angularTraderCellDTO.setHeadOfSector(headOfSector);

        responseData.setTraders(angularTraderCellDTO);

        return responseData;
    }

    @Transactional
    public String deleteTraderCell(int id) {
        return deleteTraderDao.deleteTraderCell(id);
    }

    @Transactional
    public String deletePortativCell(int id) {
        return deleteTraderDao.deletePortativCell(id);
    }

    @Transactional
    public void saveTraderCell(String json, boolean isTrader) {
        AngularSaveTraderCellDTO angularSaveTraderCellDTO = new AngularSaveTraderCellDTO();
        PostgresSaveTraderCellDTO postgresSaveTraderCellDTO = new PostgresSaveTraderCellDTO();

        if (Objects.nonNull(json) && jsonHelper.isValid(json)) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                angularSaveTraderCellDTO = objectMapper.readValue(json, AngularSaveTraderCellDTO.class);

            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }

        String code = angularSaveTraderCellDTO.getTraders().getCodeTO();
        code = "3" + code.substring(1);

        postgresSaveTraderCellDTO.setMainKey(code);
        postgresSaveTraderCellDTO.setCreateDate(angularSaveTraderCellDTO.getDate());
        postgresSaveTraderCellDTO.setTitleOrder(angularSaveTraderCellDTO.getOrder());

        postgresSaveTraderCellDTO.setCodeTo(angularSaveTraderCellDTO.getTraders().getCodeTO());
        postgresSaveTraderCellDTO.setCity(angularSaveTraderCellDTO.getTraders().getCity());
        postgresSaveTraderCellDTO.setAddress(angularSaveTraderCellDTO.getTraders().getAddress());
        postgresSaveTraderCellDTO.setPlace(angularSaveTraderCellDTO.getTraders().getPlace());
        postgresSaveTraderCellDTO.setMobileOperator(angularSaveTraderCellDTO.getTraders().getOperator());
        postgresSaveTraderCellDTO.setSchedule(angularSaveTraderCellDTO.getTraders().getSchedule());
        postgresSaveTraderCellDTO.setPhone(angularSaveTraderCellDTO.getTraders().getPhone());
        postgresSaveTraderCellDTO.setHeadOfSector(angularSaveTraderCellDTO.getTraders().getHeadOfSector());

        saveOfficeCellDao.saveTraderCell(PostgresSaveTraderCellDTO.builder()
                .mainKey(postgresSaveTraderCellDTO.getMainKey())
                .createDate(postgresSaveTraderCellDTO.getCreateDate())
                .titleOrder(postgresSaveTraderCellDTO.getTitleOrder())
                .codeTo(postgresSaveTraderCellDTO.getCodeTo())
                .city(postgresSaveTraderCellDTO.getCity())
                .address(postgresSaveTraderCellDTO.getAddress())
                .place(postgresSaveTraderCellDTO.getPlace())
                .mobileOperator(postgresSaveTraderCellDTO.getMobileOperator())
                .schedule(postgresSaveTraderCellDTO.getSchedule())
                .phone(postgresSaveTraderCellDTO.getPhone())
                .headOfSector(postgresSaveTraderCellDTO.getHeadOfSector())
                .build(), isTrader);
    }

    @Transactional
    public void updateTraderCellToPostgres(String json, boolean isTrader) {
        AngularSaveTraderCellDTO angularSaveTraderCellDTO = new AngularSaveTraderCellDTO();
        PostgresUpdateTradeCellDTO postgresUpdateTradeCellDTO = new PostgresUpdateTradeCellDTO();

        if (Objects.nonNull(json) && jsonHelper.isValid(json)) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                angularSaveTraderCellDTO = objectMapper.readValue(json, AngularSaveTraderCellDTO.class);

            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }

        String procedureName = "";

        if (isTrader) {
            procedureName = "update_trader_cell";
        } else {
            procedureName = "update_portativ_cell";
        }

        String code = angularSaveTraderCellDTO.getTraders().getCodeTO();
        code = "3" + code.substring(1);

        postgresUpdateTradeCellDTO.setId(Long.valueOf(angularSaveTraderCellDTO.getId()));
        postgresUpdateTradeCellDTO.setMainKey(code);
        postgresUpdateTradeCellDTO.setCreateDate(angularSaveTraderCellDTO.getDate());
        postgresUpdateTradeCellDTO.setTitleOrder(angularSaveTraderCellDTO.getOrder());

        postgresUpdateTradeCellDTO.setCodeTo(angularSaveTraderCellDTO.getTraders().getCodeTO());
        postgresUpdateTradeCellDTO.setCity(angularSaveTraderCellDTO.getTraders().getCity());
        postgresUpdateTradeCellDTO.setAddress(angularSaveTraderCellDTO.getTraders().getAddress());
        postgresUpdateTradeCellDTO.setPlace(angularSaveTraderCellDTO.getTraders().getPlace());
        postgresUpdateTradeCellDTO.setMobileOperator(angularSaveTraderCellDTO.getTraders().getOperator());
        postgresUpdateTradeCellDTO.setSchedule(angularSaveTraderCellDTO.getTraders().getSchedule());
        postgresUpdateTradeCellDTO.setPhone(angularSaveTraderCellDTO.getTraders().getPhone());
        postgresUpdateTradeCellDTO.setHeadOfSector(angularSaveTraderCellDTO.getTraders().getHeadOfSector());

        updateOfficeCellDao.updateTraderCell(PostgresUpdateTradeCellDTO.builder()
                .id(postgresUpdateTradeCellDTO.getId())
                .mainKey(postgresUpdateTradeCellDTO.getMainKey())
                .createDate(postgresUpdateTradeCellDTO.getCreateDate())
                .titleOrder(postgresUpdateTradeCellDTO.getTitleOrder())
                .codeTo(postgresUpdateTradeCellDTO.getCodeTo())
                .city(postgresUpdateTradeCellDTO.getCity())
                .address(postgresUpdateTradeCellDTO.getAddress())
                .place(postgresUpdateTradeCellDTO.getPlace())
                .mobileOperator(postgresUpdateTradeCellDTO.getMobileOperator())
                .schedule(postgresUpdateTradeCellDTO.getSchedule())
                .phone(postgresUpdateTradeCellDTO.getPhone())
                .headOfSector(postgresUpdateTradeCellDTO.getHeadOfSector())
                .build(), procedureName);
    }

    public Map<String, AsterixDTO> GetAsterix() {
        Map<String, AsterixDTO> responseDataMap = new HashMap<>();
        ResponseList<PostgresGetTraderCellDTO> bdResponse = officeCellService.getTraderCell();
        ResponseList<PostgresGetTraderCellDTO> bdResponsePortativ = officeCellService.getPortativCell();
        List<PostgresGetTraderCellDTO> bdList = bdResponse.getContent(); // лист с данными из бд
        List<PostgresGetTraderCellDTO> bdListPortativ = bdResponsePortativ.getContent();

        for (PostgresGetTraderCellDTO cellDTO : bdList) {
            AsterixDTO responseData = new AsterixDTO();
            responseData.setTo(cellDTO.getCodeTo());
            responseData.setPhone(cellDTO.getPhone());
            responseDataMap.put(cellDTO.getMainKey(), responseData);
        }

        for (PostgresGetTraderCellDTO cellDTO : bdListPortativ) {
            AsterixDTO responseData = new AsterixDTO();
            responseData.setTo(cellDTO.getCodeTo());
            responseData.setPhone(cellDTO.getPhone());
            responseDataMap.put(cellDTO.getMainKey(), responseData);
        }

        return responseDataMap;
    }

    public List<PostgresGetTraderCellDTO> removeDuplicates(List<PostgresGetTraderCellDTO> bdList) {
        Set<String> uniqueKeys = new HashSet<>();
        return bdList.stream()
                .filter(dto -> uniqueKeys.add(dto.getMainKey()))
                .sorted(Comparator.comparing(PostgresGetTraderCellDTO::getMainKey))
                .collect(Collectors.toList());
    }

    public List<PostgresGetTraderCellDTO> mergeAndProcessLists(List<PostgresGetTraderCellDTO> bdList, List<PostgresGetTraderCellDTO> bdListPortativ) {
        List<PostgresGetTraderCellDTO> combinedList = new ArrayList<>(bdList);
        combinedList.addAll(bdListPortativ);
        return removeDuplicates(combinedList);
    }

    public List<String> GetAsterixPars() {
        String template = "exten => %s,1,Dial(Local/%s@${CONTEXT})";

        List<String> asterixList = new ArrayList<>();
        ResponseList<PostgresGetTraderCellDTO> bdResponse = officeCellService.getTraderCell();
        List<PostgresGetTraderCellDTO> bdList = bdResponse.getContent(); // лист с данными из бд
        ResponseList<PostgresGetTraderCellDTO> bdResponsePortativ = officeCellService.getPortativCell();
        List<PostgresGetTraderCellDTO> bdListPortativ = bdResponsePortativ.getContent();
        List<PostgresGetTraderCellDTO> bdListMerged = mergeAndProcessLists(bdList, bdListPortativ);

        for (PostgresGetTraderCellDTO cellDTO : bdListMerged) {
            if (cellDTO.getPhone().equals("375 29 303 03 03")) {
                continue;
            }

            if (cellDTO.getMainKey().startsWith("9")) {
                continue;
            } else {
                String phone = cellDTO.getPhone();

                phone = phone.replaceAll("\\s+", "").split("<br>")[0];
                phone = phone.replaceFirst("^375", "80");
                String shortPhone = cellDTO.getMainKey();
                String asterixLine = String.format(template, shortPhone, phone);
                asterixList.add(asterixLine);
            }
        }
        return asterixList;
    }
}

package by.zyskunov.RestApiPrc.services;

import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresGetOfficeCellDTO;
import by.zyskunov.RestApiPrc.utils.ResponseList;

public interface IOfficeCellService {
    ResponseList<PostgresGetOfficeCellDTO> getOfficeCell(Long id);
    String deleteOfficeCell(int id);
}

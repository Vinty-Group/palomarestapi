package by.zyskunov.RestApiPrc.services;

import by.zyskunov.RestApiPrc.dao.DeleteOfficeDao;
import by.zyskunov.RestApiPrc.dao.GetOfficeCellDao;
import by.zyskunov.RestApiPrc.dao.SaveOfficeCellDao;
import by.zyskunov.RestApiPrc.dao.UpdateOfficeCellDao;
import by.zyskunov.RestApiPrc.models.OfficeCell.AngularSaveOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresGetOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresSaveOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresUpdateOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.PartnersCell.PostgresGetPartnersCellDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.PostgresGetTraderCellDTO;
import by.zyskunov.RestApiPrc.utils.JsonHelper;
import by.zyskunov.RestApiPrc.utils.ResponseList;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.SecureRandom;
import java.util.Objects;

@Log4j2
@Service
@RequiredArgsConstructor
public class OfficeCellServiceImpl {
    private final GetOfficeCellDao getOfficeCellDao;
    private final DeleteOfficeDao deleteOfficeDao;
    private final SaveOfficeCellDao saveOfficeCellDao;
    private final UpdateOfficeCellDao updateOfficeCellDao;
    private final JsonHelper jsonHelper;

    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final int LENGTH = 15;

    @Transactional(readOnly = true) // для офисцел
    public ResponseList<PostgresGetOfficeCellDTO> getOfficeCell() {
        return getOfficeCellDao.getOfficeCell();
    }

    @Transactional(readOnly = true) // для трейд цел
    public ResponseList<PostgresGetTraderCellDTO> getTraderCell() {
        return getOfficeCellDao.getTraderCell();
    }

    @Transactional(readOnly = true) // для portativ цел
    public ResponseList<PostgresGetTraderCellDTO> getPortativCell() {
        return getOfficeCellDao.getTPortativCell();
    }

    @Transactional(readOnly = true)
    public ResponseList<PostgresGetPartnersCellDTO> getPartnersCell() {
        return getOfficeCellDao.getPartnerCell();
    }

    @Transactional
    public String deleteOfficeCell(int id) {
        return deleteOfficeDao.deleteOfficeCell(id);
    }

    @Transactional
    public void saveOfficeCellToPostgres(String json)
    {
        AngularSaveOfficeCellDTO angularSaveOfficeCellDTO = new AngularSaveOfficeCellDTO();
                PostgresSaveOfficeCellDTO officeCellDTO = new PostgresSaveOfficeCellDTO();

        if (Objects.nonNull(json) && jsonHelper.isValid(json))
        {
            ObjectMapper objectMapper = new ObjectMapper();

            try
            {
                angularSaveOfficeCellDTO = objectMapper.readValue(json, AngularSaveOfficeCellDTO.class);

            } catch (JsonProcessingException e)
            {
                throw new RuntimeException(e);
            }
        }

        String randomId = generateRandomString();

        officeCellDTO.setMainKey(randomId);
        officeCellDTO.setCreateDate(angularSaveOfficeCellDTO.getDate());

        officeCellDTO.setFullName(angularSaveOfficeCellDTO.getOfficeCells().getFullName());
        officeCellDTO.setPhone(angularSaveOfficeCellDTO.getOfficeCells().getPhone());
        officeCellDTO.setJobTitle(angularSaveOfficeCellDTO.getOfficeCells().getJobTitle());
        officeCellDTO.setResponsibleFor(angularSaveOfficeCellDTO.getOfficeCells().getResponsibleFor());

        officeCellDTO.setTitleOrder(angularSaveOfficeCellDTO.getOrder());
        officeCellDTO.setParentKey(angularSaveOfficeCellDTO.getParent());
        officeCellDTO.setTypeRecord(angularSaveOfficeCellDTO.getTypeRecord());
        officeCellDTO.setFiredStatus(angularSaveOfficeCellDTO.getFiredStatus());

                 saveOfficeCellDao.saveOfficeCell(PostgresSaveOfficeCellDTO.builder()
                .mainKey(officeCellDTO.getMainKey())
                .createDate(officeCellDTO.getCreateDate())
                .fullName(officeCellDTO.getFullName())
                .jobTitle(officeCellDTO.getJobTitle())
                .responsibleFor(officeCellDTO.getResponsibleFor())
                .phone(officeCellDTO.getPhone())
                .titleOrder(officeCellDTO.getTitleOrder())
                .parentKey(officeCellDTO.getParentKey())
                .typeRecord(officeCellDTO.getTypeRecord())
                .firedStatus(officeCellDTO.getFiredStatus())
                .build());
    }

    @Transactional
    public void updateOfficeCellToPostgres(String json)
    {
        AngularSaveOfficeCellDTO angularSaveOfficeCellDTO = new AngularSaveOfficeCellDTO();
        PostgresUpdateOfficeCellDTO postgresUpdateOfficeCellDTO = new PostgresUpdateOfficeCellDTO();

        if (Objects.nonNull(json) && jsonHelper.isValid(json))
        {
            ObjectMapper objectMapper = new ObjectMapper();

            try
            {
                angularSaveOfficeCellDTO = objectMapper.readValue(json, AngularSaveOfficeCellDTO.class);

            } catch (JsonProcessingException e)
            {
                throw new RuntimeException(e);
            }
        }

        String randomId = generateRandomString();

        postgresUpdateOfficeCellDTO.setMainKey(randomId);
        postgresUpdateOfficeCellDTO.setId(Long.valueOf(angularSaveOfficeCellDTO.getId()));
        postgresUpdateOfficeCellDTO.setCreateDate(angularSaveOfficeCellDTO.getDate());

        postgresUpdateOfficeCellDTO.setFullName(angularSaveOfficeCellDTO.getOfficeCells().getFullName());
        postgresUpdateOfficeCellDTO.setPhone(angularSaveOfficeCellDTO.getOfficeCells().getPhone());
        postgresUpdateOfficeCellDTO.setJobTitle(angularSaveOfficeCellDTO.getOfficeCells().getJobTitle());
        postgresUpdateOfficeCellDTO.setResponsibleFor(angularSaveOfficeCellDTO.getOfficeCells().getResponsibleFor());

        postgresUpdateOfficeCellDTO.setTitleOrder(angularSaveOfficeCellDTO.getOrder());
        postgresUpdateOfficeCellDTO.setParentKey(angularSaveOfficeCellDTO.getParent());
        postgresUpdateOfficeCellDTO.setTypeRecord(angularSaveOfficeCellDTO.getTypeRecord());
        postgresUpdateOfficeCellDTO.setFiredStatus(angularSaveOfficeCellDTO.getFiredStatus());


            ResponseList<PostgresUpdateOfficeCellDTO> resultList = updateOfficeCellDao.updateOfficeCell(PostgresUpdateOfficeCellDTO.builder()
                    .id(postgresUpdateOfficeCellDTO.getId())
                    .mainKey(postgresUpdateOfficeCellDTO.getMainKey())
                    .createDate(postgresUpdateOfficeCellDTO.getCreateDate())
                    .fullName(postgresUpdateOfficeCellDTO.getFullName())
                    .jobTitle(postgresUpdateOfficeCellDTO.getJobTitle())
                    .phone(postgresUpdateOfficeCellDTO.getPhone())
                    .responsibleFor(postgresUpdateOfficeCellDTO.getResponsibleFor())
                    .titleOrder(postgresUpdateOfficeCellDTO.getTitleOrder())
                    .parentKey(postgresUpdateOfficeCellDTO.getParentKey())
                    .typeRecord(postgresUpdateOfficeCellDTO.getTypeRecord())
                    .firedStatus(postgresUpdateOfficeCellDTO.getFiredStatus())
                    .build());
    }

    public static String generateRandomString()
    {
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder(LENGTH);

        for (int i = 0; i < LENGTH; i++) {
            int index = random.nextInt(CHARACTERS.length());
            sb.append(CHARACTERS.charAt(index));
        }

        return sb.toString();
    }
}

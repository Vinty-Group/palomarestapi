package by.zyskunov.RestApiPrc.models.OfficeCell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
// дто что бы положить данные в бд
public class PostgresSaveOfficeCellDTO {
    private String mainKey;
    private String createDate;
    private String fullName;
    private String jobTitle;
    private String phone;
    private String responsibleFor;
    private Long titleOrder;
    private String parentKey;
    private String typeRecord;
    private Long firedStatus;
}

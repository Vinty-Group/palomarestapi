package by.zyskunov.RestApiPrc.models.OfficeCell;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
// то что пойдет от контроллера в ангулар
public class AngularSaveOfficeCellDTO {
    private String id;
    private String typeRecord;
    private AngularOfficeCellDTO officeCells;
    private String date;
    private String parent;
    private Long order;
    private Long firedStatus;
}

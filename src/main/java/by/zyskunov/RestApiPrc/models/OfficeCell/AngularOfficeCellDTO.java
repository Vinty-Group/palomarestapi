package by.zyskunov.RestApiPrc.models.OfficeCell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

//дто для вложенного обьекта, что бы джейсон был с такой структурой (нужно для ангулара)
public class AngularOfficeCellDTO {
    private String fullName;
    private String jobTitle;
    private String phone;
    private String responsibleFor;
}

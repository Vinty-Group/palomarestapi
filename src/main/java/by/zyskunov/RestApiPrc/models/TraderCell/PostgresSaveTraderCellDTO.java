package by.zyskunov.RestApiPrc.models.TraderCell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
// дто что бы положить данные в бд
public class PostgresSaveTraderCellDTO {
    private String mainKey;
    private String createDate;
    private Long titleOrder;
    private String codeTo;
    private String city;
    private String address;
    private String place;
    private String mobileOperator;
    private String schedule;
    private String phone;
    private String headOfSector;
}

package by.zyskunov.RestApiPrc.models.TraderCell;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
// для получения из бд тут вся структура как в бд должна быть
public class PostgresGetTraderCellDTO {
    private Long id;
    private String mainKey;
    private String createDate;
    private Long titleOrder;
    private String codeTo;
    private String city;
    private String address;
    private String place;
    private String mobileOperator;
    private String schedule;
    private String phone;
    private String headOfSector;
}

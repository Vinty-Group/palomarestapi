package by.zyskunov.RestApiPrc.models.TraderCell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

// дто для апдейта в сторону бд - то что будет ложится в бд с такой структурой
public class PostgresUpdateTradeCellDTO {
    private Long id;
    private String mainKey;
    private String createDate;
    private Long titleOrder;
    private String codeTo;
    private String city;
    private String address;
    private String place;
    private String mobileOperator;
    private String schedule;
    private String phone;
    private String headOfSector;
}

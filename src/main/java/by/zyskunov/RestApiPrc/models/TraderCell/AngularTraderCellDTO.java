package by.zyskunov.RestApiPrc.models.TraderCell;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

//дто для вложенного Объекта, что бы Джейсон был с такой структурой (нужно для ангулара)
public class AngularTraderCellDTO {
    private String codeTO;
    private String city;
    private String address;
    private String place;
    private String operator;
    private String schedule;
    private String phone;
    private String headOfSector;
}

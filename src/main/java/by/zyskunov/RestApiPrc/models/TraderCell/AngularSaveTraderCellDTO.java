package by.zyskunov.RestApiPrc.models.TraderCell;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
// то что пойдет от контроллера в ангулар и из ангулара тоже
public class AngularSaveTraderCellDTO  {
    private String id;
    private AngularTraderCellDTO traders;
    private String date;
    private Long order;
}

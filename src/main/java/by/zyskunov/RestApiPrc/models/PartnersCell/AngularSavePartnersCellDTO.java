package by.zyskunov.RestApiPrc.models.PartnersCell;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class AngularSavePartnersCellDTO {
    private Long id;
    private AngularPartnersCellDTO partners;
    private String date;
    private Long order;
}

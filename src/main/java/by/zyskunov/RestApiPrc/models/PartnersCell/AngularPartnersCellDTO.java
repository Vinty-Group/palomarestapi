package by.zyskunov.RestApiPrc.models.PartnersCell;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AngularPartnersCellDTO {
    private String serviceName;
    private String bankName;
    private String fullName;
    private String phone;
    private String description;
}

package by.zyskunov.RestApiPrc.dao;

import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresGetOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.PartnersCell.PostgresGetPartnersCellDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.PostgresGetTraderCellDTO;
import by.zyskunov.RestApiPrc.procedures.GetOfficeCellPRC;
import by.zyskunov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Log4j2
@Repository
@RequiredArgsConstructor
// Слой DAO. Лезет в БД. Вызывает процедуры. Дает на вход и выход параметры необходимые для ее работы
public class GetOfficeCellDao {
    private final JdbcTemplate jdbcTemplate;
    private final GetOfficeCellPRC getOfficeCellPRC;


    public ResponseList<PostgresGetOfficeCellDTO> getOfficeCell()  // метод в дао вернет респон лист OFFICE
    {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        getOfficeCellPRC.fillOUTParametersOffice(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> result = jdbcCall.execute(); //это вернет данные от реф курсора в мапу
        List<Map<String, Object>> resultSet = (List<Map<String, Object>>) result.get("ref_cursor");

        List<PostgresGetOfficeCellDTO> list = getOfficeCellPRC.getOfficeCell(resultSet); // тут юзаем метод для парсимнга реф курсора, он вернет лист с данными
        ResponseList<PostgresGetOfficeCellDTO> responseList = new ResponseList<>();

        responseList.setContent(list);
        responseList.setRespCode("200");
        responseList.setRespMess("Данные успешно получены");
        return responseList;
    }

    public ResponseList<PostgresGetTraderCellDTO> getTraderCell()  // метод в дао вернет респон лист TRADER
    {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        getOfficeCellPRC.fillOUTParametersTrader(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> result = jdbcCall.execute(); //это вернет данные от реф курсора в мапу
        List<Map<String, Object>> resultSet = (List<Map<String, Object>>) result.get("ref_cursor");

        List<PostgresGetTraderCellDTO> list = getOfficeCellPRC.getTraderCell(resultSet); // тут юзаем метод для парсимнга реф курсора, он вернет лист с данными
        ResponseList<PostgresGetTraderCellDTO> responseList = new ResponseList<>();

        responseList.setContent(list);
        responseList.setRespCode("200");
        responseList.setRespMess("Данные успешно получены");
        return responseList;
    }

    public ResponseList<PostgresGetTraderCellDTO> getTPortativCell() {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        getOfficeCellPRC.fillOUTParametersPortativ(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> result = jdbcCall.execute(); //это вернет данные от реф курсора в мапу
        List<Map<String, Object>> resultSet = (List<Map<String, Object>>) result.get("ref_cursor");

        List<PostgresGetTraderCellDTO> list = getOfficeCellPRC.getTraderCell(resultSet); // тут юзаем метод для парсимнга реф курсора, он вернет лист с данными
        ResponseList<PostgresGetTraderCellDTO> responseList = new ResponseList<>();

        responseList.setContent(list);
        responseList.setRespCode("200");
        responseList.setRespMess("Данные успешно получены");
        return responseList;
    }

    public ResponseList<PostgresGetPartnersCellDTO> getPartnerCell()  // метод в дао вернет респон лист TRADER
    {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        getOfficeCellPRC.fillOUTParametersPartners(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> result = jdbcCall.execute(); //это вернет данные от реф курсора в мапу
        List<Map<String, Object>> resultSet = (List<Map<String, Object>>) result.get("ref_cursor");

        List<PostgresGetPartnersCellDTO> list = getOfficeCellPRC.getPartnersCell(resultSet); // тут юзаем метод для парсимнга реф курсора, он вернет лист с данными
        ResponseList<PostgresGetPartnersCellDTO> responseList = new ResponseList<>();

        responseList.setContent(list);
        responseList.setRespCode("200");
        responseList.setRespMess("Данные успешно получены");
        return responseList;
    }

}

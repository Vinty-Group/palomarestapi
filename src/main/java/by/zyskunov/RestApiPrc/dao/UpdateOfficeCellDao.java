package by.zyskunov.RestApiPrc.dao;

import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresUpdateOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.PartnersCell.PostgresUpdatePartnersCellDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.PostgresUpdateTradeCellDTO;
import by.zyskunov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@Repository
@RequiredArgsConstructor
// Слой DAO. Лезет в БД. Вызывает процедуры. Дает на вход и выход параметры необходимые для ее работы

public class UpdateOfficeCellDao {
    private final JdbcTemplate jdbcTemplate;

    @Transactional
    public ResponseList<PostgresUpdateOfficeCellDTO> updateOfficeCell(PostgresUpdateOfficeCellDTO officeCellDTO) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("user_directory")
                .withProcedureName("update_office_cell")
                .declareParameters(
                        new SqlParameter("p_id", Types.BIGINT),
                        new SqlParameter("p_mainkey", Types.CHAR),
                        new SqlParameter("p_createdate", Types.CHAR),
                        new SqlParameter("p_fullname", Types.CHAR),
                        new SqlParameter("p_jobtitle", Types.VARCHAR),
                        new SqlParameter("p_phone", Types.CHAR),
                        new SqlParameter("p_responsiblefor", Types.CHAR),
                        new SqlParameter("p_titleorder", Types.BIGINT),
                        new SqlParameter("p_parentkey", Types.CHAR),
                        new SqlParameter("p_typerecord", Types.CHAR),
                        new SqlOutParameter("p_resp_code", Types.VARCHAR),
                        new SqlOutParameter("p_resp_message", Types.VARCHAR),
                        new SqlParameter("p_firedstatus", Types.BIGINT)
                );

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("p_id", officeCellDTO.getId());
        parameters.put("p_mainkey", officeCellDTO.getMainKey());
        parameters.put("p_createdate", officeCellDTO.getCreateDate());
        parameters.put("p_fullname", officeCellDTO.getFullName());
        parameters.put("p_jobtitle", officeCellDTO.getJobTitle());
        parameters.put("p_phone", officeCellDTO.getPhone());
        parameters.put("p_responsiblefor", officeCellDTO.getResponsibleFor());
        parameters.put("p_titleorder", officeCellDTO.getTitleOrder());
        parameters.put("p_parentkey", officeCellDTO.getParentKey());
        parameters.put("p_typerecord", officeCellDTO.getTypeRecord());
        parameters.put("p_firedstatus", officeCellDTO.getFiredStatus());

        Map<String, Object> result = jdbcCall.execute(parameters);

        ResponseList<PostgresUpdateOfficeCellDTO> responseList = new ResponseList<>();
        responseList.setRespCode((String) result.get("p_resp_code"));
        responseList.setRespMess((String) result.get("p_resp_message"));

        return responseList;
    }

    @Transactional
    public ResponseList<PostgresUpdateTradeCellDTO> updateTraderCell(PostgresUpdateTradeCellDTO updateTradeCellDTO, String procedureName) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("user_directory")
                .withProcedureName(procedureName)
                .declareParameters(
                        new SqlParameter("p_id", Types.BIGINT),
                        new SqlParameter("p_mainkey", Types.CHAR),
                        new SqlParameter("p_createdate", Types.CHAR),
                        new SqlParameter("p_titleorder", Types.BIGINT),
                        new SqlParameter("p_codeto", Types.CHAR),
                        new SqlParameter("p_city", Types.CHAR),
                        new SqlParameter("p_address", Types.CHAR),
                        new SqlParameter("p_place", Types.CHAR),
                        new SqlParameter("p_mobileoperator", Types.CHAR),
                        new SqlParameter("p_schedule", Types.CHAR),
                        new SqlParameter("p_phone", Types.CHAR),
                        new SqlParameter("p_headofsector", Types.CHAR),
                        new SqlOutParameter("p_resp_code", Types.VARCHAR),
                        new SqlOutParameter("p_resp_message", Types.VARCHAR)

                );

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("p_id", updateTradeCellDTO.getId());
        parameters.put("p_mainkey", updateTradeCellDTO.getMainKey());
        parameters.put("p_createdate", updateTradeCellDTO.getCreateDate());
        parameters.put("p_titleorder", updateTradeCellDTO.getTitleOrder());
        parameters.put("p_codeto", updateTradeCellDTO.getCodeTo());
        parameters.put("p_city", updateTradeCellDTO.getCity());
        parameters.put("p_address", updateTradeCellDTO.getAddress());
        parameters.put("p_place", updateTradeCellDTO.getPlace());
        parameters.put("p_mobileoperator", updateTradeCellDTO.getMobileOperator());
        parameters.put("p_schedule", updateTradeCellDTO.getSchedule());
        parameters.put("p_phone", updateTradeCellDTO.getPhone());
        parameters.put("p_headofsector", updateTradeCellDTO.getHeadOfSector());

        Map<String, Object> result = jdbcCall.execute(parameters);

        ResponseList<PostgresUpdateTradeCellDTO> responseList = new ResponseList<>();
        responseList.setRespCode((String) result.get("p_resp_code"));
        responseList.setRespMess((String) result.get("p_resp_message"));

        return responseList;
    }

    @Transactional
    public ResponseList<PostgresUpdateTradeCellDTO> updatePartnersCell(PostgresUpdatePartnersCellDTO updatePartnersCellDTO)
    {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("user_directory")
                .withProcedureName("update_partners_cell")
                .declareParameters(
                        new SqlParameter("p_id", Types.BIGINT),
                        new SqlParameter("p_createdate", Types.CHAR),
                        new SqlParameter("p_titleorder", Types.BIGINT),
                        new SqlParameter("p_servicename", Types.CHAR),
                        new SqlParameter("p_bankname", Types.CHAR),
                        new SqlParameter("p_fullname", Types.CHAR),
                        new SqlParameter("p_phone", Types.CHAR),
                        new SqlParameter("p_description", Types.CHAR),
                        new SqlOutParameter("p_resp_code", Types.VARCHAR),
                        new SqlOutParameter("p_resp_message", Types.VARCHAR)
                );

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("p_id", updatePartnersCellDTO.getId());
        parameters.put("p_createdate", updatePartnersCellDTO.getCreateDate());
        parameters.put("p_titleorder", updatePartnersCellDTO.getTitleOrder());
        parameters.put("p_servicename", updatePartnersCellDTO.getServiceName());
        parameters.put("p_bankname", updatePartnersCellDTO.getBankName());
        parameters.put("p_fullname", updatePartnersCellDTO.getFullName());
        parameters.put("p_phone", updatePartnersCellDTO.getPhone());
        parameters.put("p_description", updatePartnersCellDTO.getDescription());
        Map<String, Object> result = jdbcCall.execute(parameters);

        ResponseList<PostgresUpdateTradeCellDTO> responseList = new ResponseList<>();
        responseList.setRespCode((String) result.get("p_resp_code"));
        responseList.setRespMess((String) result.get("p_resp_message"));

        return responseList;
    }
}

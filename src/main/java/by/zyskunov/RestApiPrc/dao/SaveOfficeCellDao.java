package by.zyskunov.RestApiPrc.dao;

import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresSaveOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.PartnersCell.PostgresSavePartnersCellDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.PostgresSaveTraderCellDTO;
import by.zyskunov.RestApiPrc.procedures.SaveOfficeCellPRC;
import by.zyskunov.RestApiPrc.utils.ResponseList;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Log4j2
@Repository
@RequiredArgsConstructor
// Слой DAO. Лезет в БД. Вызывает процедуры. Дает на вход и выход параметры необходимые для ее работы
public class SaveOfficeCellDao {
    private final JdbcTemplate jdbcTemplate;
    private final SaveOfficeCellPRC saveOfficeCellPRC;

    public void saveOfficeCell(PostgresSaveOfficeCellDTO mappedClass)
    {

        System.out.println(mappedClass.getFiredStatus());
        System.out.println(mappedClass.getJobTitle());
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        saveOfficeCellPRC.fillOUTParametersOffice(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> parameters = saveOfficeCellPRC.fillINParametersOffice(jdbcCall, mappedClass); // Для входных параметров если есть такие в процедуре IN

        // выполняем процедуру с параметрами
        Map<String, Object> result = jdbcCall.execute(parameters); //parameters = входные параметры если они есть у процедуры

        ResponseList<PostgresSaveOfficeCellDTO> responseList = new ResponseList<>();
        responseList.setRespCode((String) result.get("p_resp_code")); // String OUT параметр из процедуры save_users
        responseList.setRespMess((String) result.get("p_resp_message")); // String OUT параметр из процедуры save_users
    }

    public void saveTraderCell(PostgresSaveTraderCellDTO mappedClass, boolean isTrader)
    {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        saveOfficeCellPRC.fillOUTParametersTrader(jdbcCall, isTrader); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> parameters = saveOfficeCellPRC.fillINParametersTraders(jdbcCall, mappedClass); // Для входных параметров если есть такие в процедуре IN

        // выполняем процедуру с параметрами
        Map<String, Object> result = jdbcCall.execute(parameters); //parameters = входные параметры если они есть у процедуры

        ResponseList<PostgresSaveOfficeCellDTO> responseList = new ResponseList<>();
        responseList.setRespCode((String) result.get("p_resp_code")); // String OUT параметр из процедуры save_users
        responseList.setRespMess((String) result.get("p_resp_message")); // String OUT параметр из процедуры save_users
    }


    public void savePartnerCell(PostgresSavePartnersCellDTO mappedClass)
    {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate); // Умеет работать с процедурами
        saveOfficeCellPRC.fillOUTParametersPartner(jdbcCall); // заполняем параметрами, необходимыми для работы процедуры
        Map<String, Object> parameters = saveOfficeCellPRC.fillINParametersPartners(jdbcCall, mappedClass); // Для входных параметров если есть такие в процедуре IN

        // выполняем процедуру с параметрами
        Map<String, Object> result = jdbcCall.execute(parameters); //parameters = входные параметры если они есть у процедуры

        ResponseList<PostgresSaveOfficeCellDTO> responseList = new ResponseList<>();
        responseList.setRespCode((String) result.get("p_resp_code")); // String OUT параметр из процедуры save_users
        responseList.setRespMess((String) result.get("p_resp_message")); // String OUT параметр из процедуры save_users
    }
}

package by.zyskunov.RestApiPrc.dao;

import by.zyskunov.RestApiPrc.procedures.DeleteOfficeCellPRC;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

@Log4j2
@Repository
@RequiredArgsConstructor
public class DeletePartnerDao {
    private final JdbcTemplate jdbcTemplate;
    private final DeleteOfficeCellPRC deleteOfficeCellPRC;

    public String deletePartnerCell(int id) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate);
        String deleteResult = deleteOfficeCellPRC.deleteOfficeCell(jdbcCall, id, "delete_partner_cell");
        return deleteResult;
    }
}

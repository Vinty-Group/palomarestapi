package by.zyskunov.RestApiPrc.dao;

import by.zyskunov.RestApiPrc.procedures.DeleteOfficeCellPRC;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

@Log4j2
@Repository
@RequiredArgsConstructor
public class DeleteTraderDao {
    private final JdbcTemplate jdbcTemplate;
    private final DeleteOfficeCellPRC deleteOfficeCellPRC;

    public String deleteTraderCell(int id) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate);
        String deleteResult = deleteOfficeCellPRC.deleteOfficeCell(jdbcCall, id, "delete_trader_cell");
        return deleteResult;
    }

    public String deletePortativCell(int id) {
        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate);
        String deleteResult = deleteOfficeCellPRC.deleteOfficeCell(jdbcCall, id, "delete_portativ_cell");
        return deleteResult;
    }
}

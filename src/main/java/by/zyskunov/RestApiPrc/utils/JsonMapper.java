//package by.ushakov.RestApiPrc.utils;
//
//
//import by.ushakov.RestApiPrc.exceptions.UserServiceException;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JavaType;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.log4j.Log4j2;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.http.HttpStatus;
//import org.springframework.stereotype.Component;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.List;
//import java.util.stream.Collectors;
//
//@Component
//@RequiredArgsConstructor
//@Log4j2
//public class JsonMapper {
//    @Qualifier("objectMapper")
//    private final ObjectMapper objectMapper;
//
//    public <T> T toObject(String json, Class<T> valueType) {
//        if (json == null)
//            return null;
//        try {
//            return objectMapper.readValue(json, valueType);
//        } catch (IOException e) {
//            log.error(LoggerHelper.getMethodName() + " " + Arrays.toString(e.getStackTrace()));
//            throw new UserServiceException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to convert json to object, " + e.getMessage(), e);
//        }
//    }
//
//    public <T> T toObject(JsonNode node, Class<T> valueType) {
//        if (node == null)
//            return null;
//        try {
//            return objectMapper.treeToValue(node, valueType);
//        } catch (IOException e) {
//            log.error(LoggerHelper.getMethodName() + " " + Arrays.toString(e.getStackTrace()));
//            throw new UserServiceException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to convert json to object, " + e.getMessage(), e);
//        }
//    }
//
//    public <T> T toParametrizedObject(String json, Class<?> valueType, Class<?>... parameterClasses) {
//        if (json == null) return null;
//
//        try {
//            JavaType type = objectMapper.getTypeFactory().constructParametricType(valueType, parameterClasses);
//            return objectMapper.readValue(json, type);
//        } catch (IOException e) {
//            log.error(LoggerHelper.getMethodName() + " " + Arrays.toString(e.getStackTrace()));
//            throw new UserServiceException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to convert json to object, " + e.getMessage(), e);
//        }
//    }
//
//    public JsonNode toJsonNode(String json) {
//        if (json == null)
//            return null;
//        try {
//            return objectMapper.readTree(json);
//        } catch (IOException e) {
//            log.error(LoggerHelper.getMethodName() + " " + Arrays.toString(e.getStackTrace()));
//            throw new UserServiceException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to convert json to object, " + e.getMessage(), e);
//        }
//    }
//
//    public <T> List<T> toObjectListByNode(JsonNode path,Class<T[]> valueTypes) {
//        try {
//            if (path == null || path.isEmpty())
//                return new ArrayList<>();
//            return Arrays.stream(objectMapper
//                    .treeToValue(path, valueTypes)).collect(Collectors.toList());
//        } catch (JsonProcessingException e) {
//            log.error(LoggerHelper.getMethodName() + " " + Arrays.toString(e.getStackTrace()));
//            throw new UserServiceException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to convert json to object, " + e.getMessage(), e);
//        }
//    }
//
//    public <T> List<T> toObjectListByKey(String json, Class<T[]> valueTypes, String keyName) {
//        if (json == null)
//            return Collections.emptyList();
//        JsonNode path = this.toJsonNode(json).at(keyName);
//        return this.toObjectListByNode(path,valueTypes);
//    }
//
//    public <T> T toObjectByKey(String json, Class<T> valueType, String keyName) {
//        if (json == null)
//            return null;
//        JsonNode path = this.toJsonNode(json).at(keyName);
//        return this.toObject(path,valueType);
//    }
//
//    public String writeValueAsString(Object object) {
//        try {
//            return objectMapper.writeValueAsString(object);
//        } catch (JsonProcessingException e) {
//            log.error(LoggerHelper.getMethodName() + " " + Arrays.toString(e.getStackTrace()));
//            throw new UserServiceException(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to convert object to json, " + e.getMessage(), e);
//        }
//    }
//}

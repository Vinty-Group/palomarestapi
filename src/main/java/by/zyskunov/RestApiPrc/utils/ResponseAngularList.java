package by.zyskunov.RestApiPrc.utils;

import by.zyskunov.RestApiPrc.models.OfficeCell.AngularSaveOfficeCellDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import java.util.List;
@Data
@NoArgsConstructor
@Component
@AllArgsConstructor
@Builder

public class ResponseAngularList<T>  {
    private List<AngularSaveOfficeCellDTO> idHeader;
}

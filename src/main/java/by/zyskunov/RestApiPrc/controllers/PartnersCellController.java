package by.zyskunov.RestApiPrc.controllers;

import by.zyskunov.RestApiPrc.models.PartnersCell.AngularSavePartnersCellDTO;
import by.zyskunov.RestApiPrc.services.PartnersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.sql.SQLException;
import java.util.Map;

@Validated
@RestController
@RequiredArgsConstructor
public class PartnersCellController {
    private final PartnersService partnersService;

    @GetMapping(value = "/getPartnersCell") // контроллер partners
    public Map<String, AngularSavePartnersCellDTO> getPartnersCell() throws SQLException
    {
        return partnersService.getPartnerData();
    }

    @GetMapping(value = "/delPartner/{id}")
    public ResponseEntity<String> deletePartnersCell(@PathVariable int id)
    {
        String resultTrader = partnersService.deletePartnersCell(id);
        String jsonOk = "{\"status\": \"ok\"}";
        String jsonError = "{\"status\": \"error\"}";
        if ("ok".equals(resultTrader)) {
            return ResponseEntity.ok(jsonOk);
        } else {
            return ResponseEntity.ok(jsonError);
        }
    }

    @PostMapping(value = "/savePartnerCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void savePartnersCell(@RequestBody String json)
    {
        partnersService.savePartnersCell(json);
    }

    @PutMapping(value = "/updatePartnerCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updatePartnersCell(@RequestBody String json)
    {
        partnersService.updatePartnersCell(json);
    }
}

package by.zyskunov.RestApiPrc.controllers;

import by.zyskunov.RestApiPrc.models.Asterix.AsterixDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.AngularSaveTraderCellDTO;
import by.zyskunov.RestApiPrc.services.TraderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Validated
@RestController
@RequiredArgsConstructor
public class TraderCellController {
    private final TraderService traderService;

    @GetMapping(value = "/getTraderCell") // контроллер trader
    public Map<String, AngularSaveTraderCellDTO> getTraders() throws SQLException
    {
        return traderService.GetTradersData();
    }

    @GetMapping(value = "/getPortativCell") // контроллер getPortativCell
    public Map<String, AngularSaveTraderCellDTO> getPortitive() throws SQLException
    {
        return traderService.GetPortativData();
    }

    @GetMapping(value = "/getAsterix", produces = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")
    public Map<String, AsterixDTO> getAsterix() throws SQLException
    {
        return traderService.GetAsterix();
    }

    @GetMapping(value = "/getAsterixPars", produces = MediaType.APPLICATION_JSON_VALUE + ";charset=UTF-8")
    public List<String> getAsterixPars() throws SQLException
    {
        return traderService.GetAsterixPars();
    }

    @GetMapping(value = "/delTrader/{id}")
    public ResponseEntity<String> deleteTraderCell(@PathVariable int id)
    {
        String resultTrader = traderService.deleteTraderCell(id);
        String jsonOk = "{\"status\": \"ok\"}";
        String jsonError = "{\"status\": \"error\"}";
        if ("ok".equals(resultTrader)) {
            return ResponseEntity.ok(jsonOk);
        } else {
            return ResponseEntity.ok(jsonError);
        }
    }

    @GetMapping(value = "/delPortativ/{id}")
    public ResponseEntity<String> deletePortativCell(@PathVariable int id)
    {
        String resultTrader = traderService.deletePortativCell(id);
        String jsonOk = "{\"status\": \"ok\"}";
        String jsonError = "{\"status\": \"error\"}";
        if ("ok".equals(resultTrader)) {
            return ResponseEntity.ok(jsonOk);
        } else {
            return ResponseEntity.ok(jsonError);
        }
    }

    @PostMapping(value = "/saveTraderCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveTraderCell(@RequestBody String json)
    {
        traderService.saveTraderCell(json, true);
    }

    @PostMapping(value = "/savePortativCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void savePortativCell(@RequestBody String json)
    {
        traderService.saveTraderCell(json, false);
    }

    @PutMapping(value = "/updateTraderCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateTraderCell(@RequestBody String json)
    {
        traderService.updateTraderCellToPostgres(json, true);
    }

    @PutMapping(value = "/updatePortativCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updatePortativCell(@RequestBody String json)
    {
        traderService.updateTraderCellToPostgres(json, false);
    }
}

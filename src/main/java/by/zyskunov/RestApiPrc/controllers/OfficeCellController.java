package by.zyskunov.RestApiPrc.controllers;

import by.zyskunov.RestApiPrc.models.OfficeCell.*;
import by.zyskunov.RestApiPrc.services.AngularGetOfficeService;
import by.zyskunov.RestApiPrc.services.OfficeCellServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Map;

@Validated
@RestController
@RequiredArgsConstructor
public class OfficeCellController {
    private final OfficeCellServiceImpl officeCellService;
    private final AngularGetOfficeService angularGetOfficeService;

    @GetMapping(value = "/getOfficeCell")
    public Map<String, AngularSaveOfficeCellDTO> getOfficeAngularData() throws SQLException {
        return angularGetOfficeService.GetOfficeData();
    }

    @GetMapping(value = "/del/{id}")
    public ResponseEntity<String> deleteOfficeCell(@PathVariable int id)
    {
        String result = officeCellService.deleteOfficeCell(id);
        String jsonOk = "{\"status\": \"ok\"}";
        String jsonError = "{\"status\": \"error\"}";
        if ("ok".equals(result)) {
            return ResponseEntity.ok(jsonOk);
        } else {
            return ResponseEntity.ok(jsonError);
        }
    }

    @PostMapping(value = "/saveOfficeCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void saveOfficeCell(@RequestBody String json)
    {
        officeCellService.saveOfficeCellToPostgres(json);
    }

    @PutMapping(value = "/updateOfficeCell", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateOfficeCell(@RequestBody String json)
    {
        officeCellService.updateOfficeCellToPostgres(json);
    }
}

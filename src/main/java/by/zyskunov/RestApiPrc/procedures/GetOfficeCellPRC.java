package by.zyskunov.RestApiPrc.procedures;

import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresGetOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.PartnersCell.PostgresGetPartnersCellDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.PostgresGetTraderCellDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@Component
// описываем процедуру в БД.Где находится. Какие параметры для выполнения процедуры из слоя DAO
public class GetOfficeCellPRC {
    private final String officeCellSchemaName = "user_directory"; // схема где лежит процедура в БД
    private final String officeCellPrcName = "get_office_cell"; // название процедуры в БД
    private final String traderCellPrcName = "get_trade_cell"; // название процедуры в БД
    private final String portativCellPrcName = "get_portativ_cell"; // название процедуры в БД
    private final String partnerCellPrcName = "get_partners_cell"; // название процедуры в БД

    // названия параметров на выход процедуры
    private final String OutParamName_1 = "ref_cursor";

    // Поля RESULTSET, которые возвращает REFCURSOR. Порядок ВАЖЕН! Именно в таком виде они отображаются в SELECT.
    private final String ID = "id";
    private final String MAIN_KEY = "mainkey";
    private final String CREATE_DATE = "createdate";
    private final String FULL_NAME = "fullname";
    private final String JOB_TITLE = "jobtitle";
    private final String PHONE = "phone";
    private final String RESPON_SIBLEFOR = "responsiblefor";
    private final String TITLE_ORDER = "titleorder";
    private final String PARENT_KEY = "parentkey";
    private final String TYPE_RECORD = "typerecord";
    private final String FIRED_STATUS = "firedstatus";

    // заполняем параметрами SimpleJdbcCall. Эти параметры будут использованы при вызове процедуры.
    public void fillOUTParametersOffice(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getOfficeCellPrcName()); // Имя хранимой процедуры
        jdbcCall.declareParameters(new SqlOutParameter(OutParamName_1, Types.REF_CURSOR)); // Имя выходного параметра refcursor
    }

    public void fillOUTParametersTrader(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getTraderCellPrcName()); // Имя хранимой процедуры

        jdbcCall.declareParameters(new SqlOutParameter(OutParamName_1, Types.REF_CURSOR)); // Имя выходного параметра refcursor
    }

    public void fillOUTParametersPortativ(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getPortativCellPrcName()); // Имя хранимой процедуры
        jdbcCall.declareParameters(new SqlOutParameter(OutParamName_1, Types.REF_CURSOR)); // Имя выходного параметра refcursor
    }

    public void fillOUTParametersPartners(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getPartnerCellPrcName()); // Имя хранимой процедуры
        jdbcCall.declareParameters(new SqlOutParameter(OutParamName_1, Types.REF_CURSOR)); // Имя выходного параметра refcursor
    }

    // Маппинг. Что бы все поля которые пришли в REFCURSOR из БД - легли в объект Java.
    public List<PostgresGetOfficeCellDTO> getOfficeCell(List<Map<String, Object>> resultSet) {
        List<PostgresGetOfficeCellDTO> list = new ArrayList<>();
        for (Map<String, Object> row : resultSet) {
            list.add(PostgresGetOfficeCellDTO.builder()
                    .id((Long) row.get(ID))
                    .mainKey((String) row.get(MAIN_KEY))
                    .createDate((String) row.get(CREATE_DATE))
                    .fullName((String) row.get(FULL_NAME))
                    .jobTitle((String) row.get(JOB_TITLE))
                    .phone((String) row.get(PHONE))
                    .responsibleFor((String) row.get(RESPON_SIBLEFOR))
                    .titleOrder((Long) row.get(TITLE_ORDER))
                    .parentKey((String) row.get(PARENT_KEY))
                    .typeRecord((String) row.get(TYPE_RECORD))
                    .firedStatus((Long) row.get(FIRED_STATUS))
                    .build());
        }
        return list;
    }

    public List<PostgresGetTraderCellDTO> getTraderCell(List<Map<String, Object>> resultSet) {
        List<PostgresGetTraderCellDTO> list = new ArrayList<>();
        for (Map<String, Object> row : resultSet) {
            list.add(PostgresGetTraderCellDTO.builder()
                    .id((Long) row.get("id"))
                    .mainKey((String) row.get("mainkey"))
                    .createDate((String) row.get("createdate"))
                    .titleOrder((Long) row.get("titleorder"))
                    .codeTo((String) row.get("codeto"))
                    .city((String) row.get("city"))
                    .address((String) row.get("address"))
                    .place((String) row.get("place"))
                    .mobileOperator((String) row.get("mobileoperator"))
                    .schedule((String) row.get("schedule"))
                    .phone((String) row.get("phone"))
                    .headOfSector((String) row.get("headOfSector"))
                    .build());
        }
        return list;
    }

    public List<PostgresGetPartnersCellDTO> getPartnersCell(List<Map<String, Object>> resultSet) {
        List<PostgresGetPartnersCellDTO> list = new ArrayList<>();
        for (Map<String, Object> row : resultSet) {
            list.add(PostgresGetPartnersCellDTO.builder()
                    .id((Long) row.get("id"))
                    .createDate((String) row.get("createdate"))
                    .titleOrder((Long) row.get("titleorder"))
                    .serviceName((String) row.get("servicename"))
                    .bankName((String) row.get("bankname"))
                    .fullName((String) row.get("fullname"))
                    .phone((String) row.get("phone"))
                    .description((String) row.get("description"))

                    .build());
        }
        return list;
    }
}

package by.zyskunov.RestApiPrc.procedures;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@RequiredArgsConstructor
@Component

public class DeleteOfficeCellPRC {

    public String deleteOfficeCell(SimpleJdbcCall jdbcCall, int id, String procedureName) {
        jdbcCall.withSchemaName("user_directory"); // указываем схему базы данных
        jdbcCall.withProcedureName(procedureName); // Имя хранимой функции
        jdbcCall.declareParameters(new SqlParameter("p_id", Types.INTEGER));

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("p_id", id); // Добавляем параметр в карту
        String message = "";

        Map<String, Object> result = jdbcCall.execute(parameters);
        for (Map.Entry<String, Object> entry : result.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();

            if (value instanceof List) {
                List<?> list = (List<?>) value;
                if (!list.isEmpty() && list.get(0) instanceof Map) {
                    Map<?, ?> map = (Map<?, ?>) list.get(0);
                    if (map.containsKey("result")) {
                        message = map.get("result").toString();
                    }
                }
            }
        }
        return message;
    }
}

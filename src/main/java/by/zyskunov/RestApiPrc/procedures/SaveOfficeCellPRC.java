package by.zyskunov.RestApiPrc.procedures;

import by.zyskunov.RestApiPrc.models.OfficeCell.PostgresSaveOfficeCellDTO;
import by.zyskunov.RestApiPrc.models.PartnersCell.PostgresSavePartnersCellDTO;
import by.zyskunov.RestApiPrc.models.TraderCell.PostgresSaveTraderCellDTO;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

@Data
@RequiredArgsConstructor
@Component
// описываем процедуру в БД.Где находится. Какие параметры для выполнения процедуры из слоя DAO
public class SaveOfficeCellPRC {
    private final String officeCellSchemaName = "user_directory"; // схема где лежит процедура в БД
    private final String officeCellPrcName = "save_office_cell"; // название процедуры в БД оффис
    private final String partnerCellPrcName = "save_partner_cell"; // название процедуры в БД оффис
    private final String traderCellPrcName = "save_trader_cell"; // название процедуры в БД трейдеры
    private final String portativCellPrcName = "save_portativ_cell"; // название процедуры в БД портатив

    // названия параметров на выход процедуры
    private final String MAIN_KEY = "p_mainkey";
    private final String CREATE_DATE = "p_createdate";
    private final String FULL_NAME = "p_fullname";
    private final String JOB_TITLE = "p_jobtitle";
    private final String PHONE = "p_phone";
    private final String RESPON_SIBLEFOR = "p_responsiblefor";
    private final String TITLE_ORDER = "p_titleorder";
    private final String PARENT_KEY = "p_parentkey";
    private final String TYPE_RECORD = "p_typerecord";
    private final String FRIED_STATUS = "p_firedstatus";

    private final String OUTParamName_1 = "p_resp_code";
    private final String OUTParamName_2 = "p_resp_message";

    // заполняем параметрами SimpleJdbcCall. Эти параметры будут использованы при вызове процедуры.
    public void fillOUTParametersOffice(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getOfficeCellPrcName()); // Имя хранимой процедуры офис
        jdbcCall.declareParameters(
                new SqlOutParameter(OUTParamName_1, Types.VARCHAR),
                new SqlOutParameter(OUTParamName_2, Types.VARCHAR)
        ); // Имя выходного параметра
    }

    public void fillOUTParametersTrader(SimpleJdbcCall jdbcCall, boolean isTrader) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных

        if (isTrader)
        {
            jdbcCall.withProcedureName(getTraderCellPrcName()); // Имя хранимой процедуры трейдры
        }
        else
        {
            jdbcCall.withProcedureName(getPortativCellPrcName()); // Имя хранимой процедуры портатив
        }

        jdbcCall.declareParameters(
                new SqlOutParameter(OUTParamName_1, Types.VARCHAR),
                new SqlOutParameter(OUTParamName_2, Types.VARCHAR)
        ); // Имя выходного параметра
    }

    public void fillOUTParametersPartner(SimpleJdbcCall jdbcCall) {
        jdbcCall.withSchemaName(getOfficeCellSchemaName()); // указываем схему базы данных
        jdbcCall.withProcedureName(getPartnerCellPrcName()); // Имя хранимой процедуры partners
        jdbcCall.declareParameters(
                new SqlOutParameter(OUTParamName_1, Types.VARCHAR),
                new SqlOutParameter(OUTParamName_2, Types.VARCHAR)
        );
    }

    public Map<String, Object> fillINParametersOffice(SimpleJdbcCall jdbcCall, PostgresSaveOfficeCellDTO mappedClass) {
        Map<String, Object> parameters = new HashMap<>(); // Для входных параметров если есть такие в процедуре IN
        parameters.put(MAIN_KEY, mappedClass.getMainKey());
        parameters.put(CREATE_DATE, mappedClass.getCreateDate());
        parameters.put(FULL_NAME, mappedClass.getFullName());
        parameters.put(JOB_TITLE, mappedClass.getJobTitle());
        parameters.put(PHONE, mappedClass.getPhone());
        parameters.put(RESPON_SIBLEFOR, mappedClass.getResponsibleFor());
        parameters.put(TITLE_ORDER, mappedClass.getTitleOrder());
        parameters.put(PARENT_KEY, mappedClass.getParentKey());
        parameters.put(TYPE_RECORD, mappedClass.getTypeRecord());
        parameters.put(FRIED_STATUS, mappedClass.getFiredStatus());
        return parameters;
    }

    public Map<String, Object> fillINParametersTraders(SimpleJdbcCall jdbcCall, PostgresSaveTraderCellDTO mappedClass) {
        Map<String, Object> parameters = new HashMap<>(); // Для входных параметров если есть такие в процедуре IN
        parameters.put("p_mainkey", mappedClass.getMainKey());
        parameters.put("p_createdate", mappedClass.getCreateDate());
        parameters.put("p_titleorder", mappedClass.getTitleOrder());
        parameters.put("p_codeto", mappedClass.getCodeTo());
        parameters.put("p_city", mappedClass.getCity());
        parameters.put("p_address", mappedClass.getAddress());
        parameters.put("p_place", mappedClass.getPlace());
        parameters.put("p_mobileoperator", mappedClass.getMobileOperator());
        parameters.put("p_schedule", mappedClass.getSchedule());
        parameters.put("p_phone", mappedClass.getPhone());
        parameters.put("p_headofsector", mappedClass.getHeadOfSector());
        return parameters;
    }

    public Map<String, Object> fillINParametersPartners(SimpleJdbcCall jdbcCall, PostgresSavePartnersCellDTO mappedClass) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("p_createdate", mappedClass.getCreateDate());
        parameters.put("p_titleorder", mappedClass.getTitleOrder());
        parameters.put("p_servicename", mappedClass.getServiceName());
        parameters.put("p_bankname", mappedClass.getBankName());
        parameters.put("p_fullname", mappedClass.getFullName());
        parameters.put("p_phone", mappedClass.getPhone());
        parameters.put("p_description", mappedClass.getDescription());

        return parameters;
    }
}

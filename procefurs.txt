
-- ** ----- Функции апдейта  ** ------

-- DROP FUNCTION user_directory.delete_office_cell(int4);
-- функция апдейта для trader

CREATE OR REPLACE FUNCTION user_directory.update_trader_cell(p_id bigint, p_mainkey character varying, p_createdate character varying, p_titleorder bigint, p_codeto character varying, p_city character varying, p_address character varying, p_place character varying, p_mobileoperator character varying, p_schedule character varying, p_phone character varying, p_headofsector character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
BEGIN
    -- Update the existing record
    UPDATE user_directory.tradercell
    SET mainkey = p_mainkey,
        createdate = p_createdate,
        titleorder = p_titleorder,
        codeto = p_codeto,
        city = p_city,
        address = p_address,
        place = p_place,
        mobileoperator = p_mobileoperator,
        schedule = p_schedule,
        phone = p_phone,
        headofsector = p_headofsector
    WHERE id = p_id;

    IF FOUND THEN
        p_resp_code := 'ok';
        p_resp_message := 'Запись успешно обновлена. id = ' || p_id;
    ELSE
        p_resp_code := 'error';
        p_resp_message := 'Запись с id = ' || p_id || ' не найдена.';
    END IF;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'error';
    p_resp_message := 'Не удалось обновить запись в таблице tradercell с id = ' || p_id;

END;
$function$
;

-- DROP FUNCTION user_directory.update_portativ_cell(in int8, in varchar, in varchar, in int8, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, out varchar, out varchar);
-- функция апдейта для portativ

CREATE OR REPLACE FUNCTION user_directory.update_portativ_cell(p_id bigint, p_mainkey character varying, p_createdate character varying, p_titleorder bigint, p_codeto character varying, p_city character varying, p_address character varying, p_place character varying, p_mobileoperator character varying, p_schedule character varying, p_phone character varying, p_headofsector character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
BEGIN
    -- Update the existing record
    UPDATE user_directory.portativcell
    SET mainkey = p_mainkey,
        createdate = p_createdate,
        titleorder = p_titleorder,
        codeto = p_codeto,
        city = p_city,
        address = p_address,
        place = p_place,
        mobileoperator = p_mobileoperator,
        schedule = p_schedule,
        phone = p_phone,
        headofsector = p_headofsector
    WHERE id = p_id;

    IF FOUND THEN
        p_resp_code := 'ok';
        p_resp_message := 'Запись успешно обновлена. id = ' || p_id;
    ELSE
        p_resp_code := 'error';
        p_resp_message := 'Запись с id = ' || p_id || ' не найдена.';
    END IF;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'error';
    p_resp_message := 'Не удалось обновить запись в таблице portativcell с id = ' || p_id;

END;
$function$
;


-- DROP FUNCTION user_directory.update_partners_cell(in int8, in varchar, in int8, in varchar, in varchar, in varchar, in varchar, in varchar, out varchar, out varchar);
-- функция апдейта для partners

CREATE OR REPLACE FUNCTION user_directory.update_partners_cell(p_id bigint, p_createdate character varying, p_titleorder bigint, p_servicename character varying, p_bankname character varying, p_fullname character varying, p_phone character varying, p_description character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
BEGIN
    -- Update the existing record
    UPDATE user_directory.partnerscell
    SET createdate = p_createdate,
        titleorder = p_titleorder,
        servicename = p_servicename,
        bankname = p_bankname,
        fullname = p_fullname,
        phone = p_phone,
        description = p_description
    WHERE id = p_id;

    IF FOUND THEN
        p_resp_code := 'ok';
        p_resp_message := 'Запись успешно обновлена. id = ' || p_id;
    ELSE
        p_resp_code := 'error';
        p_resp_message := 'Запись с id = ' || p_id || ' не найдена.';
    END IF;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'error';
    p_resp_message := 'Не удалось обновить запись в таблице tradercell с id = ' || p_id;

END;
$function$
;


-- DROP FUNCTION user_directory.update_office_cell(in int8, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in int8, in varchar, in varchar, out varchar, out varchar);
-- функция апдейта для office

CREATE OR REPLACE FUNCTION user_directory.update_office_cell(p_id bigint, p_mainkey character varying, p_createdate character varying, p_fullname character varying, p_jobtitle character varying, p_phone character varying, p_responsiblefor character varying, p_titleorder bigint, p_parentkey character varying, p_typerecord character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
BEGIN
    -- Update the existing record
    UPDATE user_directory.officecell
    SET mainkey = p_mainkey,
        createdate = p_createdate,
        fullname = p_fullname,
        jobtitle = p_jobtitle,
        phone = p_phone,
        responsiblefor = p_responsiblefor,
        titleorder = p_titleorder,
        parentkey = p_parentkey,
        typerecord = p_typerecord
    WHERE id = p_id;

    IF FOUND THEN
        p_resp_code := 'ok';
        p_resp_message := 'Запись успешно обновлена. id = ' || p_id;
    ELSE
        p_resp_code := 'error';
        p_resp_message := 'Запись с id = ' || p_id || ' не найдена.';
    END IF;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'error';
    p_resp_message := 'Не удалось обновить запись в таблице officecell с id = ' || p_id;

END;
$function$
;

-- ** ----- Функции сейва  ** ------

-- DROP FUNCTION user_directory.save_trader_cell(in varchar, in varchar, in int8, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, out varchar, out varchar);

CREATE OR REPLACE FUNCTION user_directory.save_trader_cell(p_mainkey character varying, p_createdate character varying, p_titleorder bigint, p_codeto character varying, p_city character varying, p_address character varying, p_place character varying, p_mobileoperator character varying, p_schedule character varying, p_phone character varying, p_headofsector character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
DECLARE
    p_id BIGINT;
BEGIN
    -- Вставка новой записи
    INSERT INTO user_directory.tradercell 
    (
        mainkey,
        createdate,
        titleorder,
        codeto,
        city,
        address,
        place,
        mobileoperator,
        schedule,
        phone,
        headofsector
    )
    VALUES
    (
        p_mainkey,
        p_createdate,
        p_titleorder,
        p_codeto,
        p_city,
        p_address,
        p_place,
        p_mobileoperator,
        p_schedule,
        p_phone,
        p_headofsector
    )
    RETURNING id INTO p_id;

    p_resp_code := 'OK';
    p_resp_message := 'Строка сохранена успешно. id = ' || p_id;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'ERROR';
    p_resp_message := 'Не удалось сохранить запись в таблицу officecell ' || p_fullname;

END;
$function$
;


-- DROP FUNCTION user_directory.save_portativ_cell(in varchar, in varchar, in int8, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, out varchar, out varchar);

CREATE OR REPLACE FUNCTION user_directory.save_portativ_cell(p_mainkey character varying, p_createdate character varying, p_titleorder bigint, p_codeto character varying, p_city character varying, p_address character varying, p_place character varying, p_mobileoperator character varying, p_schedule character varying, p_phone character varying, p_headofsector character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
DECLARE
    p_id BIGINT;
BEGIN
    -- Вставка новой записи
    INSERT INTO user_directory.portativcell 
    (
        mainkey,
        createdate,
        titleorder,
        codeto,
        city,
        address,
        place,
        mobileoperator,
        schedule,
        phone,
        headofsector
    )
    VALUES
    (
        p_mainkey,
        p_createdate,
        p_titleorder,
        p_codeto,
        p_city,
        p_address,
        p_place,
        p_mobileoperator,
        p_schedule,
        p_phone,
        p_headofsector
    )
    RETURNING id INTO p_id;

    p_resp_code := 'OK';
    p_resp_message := 'Строка сохранена успешно. id = ' || p_id;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'ERROR';
    p_resp_message := 'Не удалось сохранить запись в таблицу officecell ' || p_fullname;

END;
$function$
;




-- DROP FUNCTION user_directory.save_partner_cell(in varchar, in int8, in varchar, in varchar, in varchar, in varchar, in varchar, out varchar, out varchar);

CREATE OR REPLACE FUNCTION user_directory.save_partner_cell(p_createdate character varying, p_titleorder bigint, p_servicename character varying, p_bankname character varying, p_fullname character varying, p_phone character varying, p_description character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
DECLARE
    p_id BIGINT;
BEGIN
    -- Вставка новой записи
    INSERT INTO user_directory.partnerscell 
    (
        createdate,
        titleorder,
        servicename,
        bankname,
        fullname,
        phone,
        description
    )
    VALUES
    (
        p_createdate,
        p_titleorder,
        p_servicename,
        p_bankname,
        p_fullname,
        p_phone,
        p_description
    )
    RETURNING id INTO p_id;

    p_resp_code := 'OK';
    p_resp_message := 'Строка сохранена успешно. id = ' || p_id;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'ERROR';
    p_resp_message := 'Не удалось сохранить запись в таблицу partnerscell ' || p_servicename;

END;
$function$
;


-- DROP FUNCTION user_directory.save_office_cell(in varchar, in varchar, in varchar, in varchar, in varchar, in varchar, in int8, in varchar, in varchar, out varchar, out varchar);

CREATE OR REPLACE FUNCTION user_directory.save_office_cell(p_mainkey character varying, p_createdate character varying, p_fullname character varying, p_jobtitle character varying, p_phone character varying, p_responsiblefor character varying, p_titleorder bigint, p_parentkey character varying, p_typerecord character varying, OUT p_resp_code character varying, OUT p_resp_message character varying)
 RETURNS record
 LANGUAGE plpgsql
AS $function$
DECLARE
    p_id BIGINT;
BEGIN
    -- Вставка новой записи
    INSERT INTO user_directory.officecell 
    (
        mainkey,
        createdate,
        fullname,
        jobtitle,
        phone,
        responsiblefor,
        titleorder,
        parentkey,
        typerecord
    )
    VALUES
    (
        p_mainkey,
        p_createdate,
        p_fullname,
        p_jobtitle,
        p_phone,
        p_responsiblefor,
        p_titleorder,
        p_parentkey,
        p_typerecord
    )
    RETURNING id INTO p_id;

    p_resp_code := 'OK';
    p_resp_message := 'Строка сохранена успешно. id = ' || p_id;

EXCEPTION WHEN OTHERS THEN
    p_resp_code := 'ERROR';
    p_resp_message := 'Не удалось сохранить запись в таблицу officecell ' || p_fullname;

END;
$function$
;


-- ** ----- Функции гет  ** ------

-- DROP FUNCTION user_directory.get_trade_cell(out refcursor);

CREATE OR REPLACE FUNCTION user_directory.get_trade_cell(OUT ref_cursor refcursor)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

    open ref_cursor for
select
  o.id,
  o.mainkey,
  o.createdate,
  o.titleorder,
  o.codeto,
  o.city,
  o.address,
  o.place,
  o.mobileoperator,
  o.schedule,
  o.phone,
  o.headofsector

from
  user_directory.tradercell o;
end;
$function$
;

-- DROP FUNCTION user_directory.get_portativ_cell(out refcursor);

CREATE OR REPLACE FUNCTION user_directory.get_portativ_cell(OUT ref_cursor refcursor)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

    open ref_cursor for
select
  o.id,
  o.mainkey,
  o.createdate,
  o.titleorder,
  o.codeto,
  o.city,
  o.address,
  o.place,
  o.mobileoperator,
  o.schedule,
  o.phone,
  o.headofsector

from
  user_directory.portativcell o;
end;
$function$
;

-- DROP FUNCTION user_directory.get_partners_cell(out refcursor);

CREATE OR REPLACE FUNCTION user_directory.get_partners_cell(OUT ref_cursor refcursor)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

    open ref_cursor for
select
  o.id,
  o.createdate,
  o.titleorder,
  o.servicename,
  o.bankname,
  o.fullname,
  o.phone,
  o.description

from
  user_directory.partnerscell o;
end;
$function$
;

-- DROP FUNCTION user_directory.get_office_cell(out refcursor);

CREATE OR REPLACE FUNCTION user_directory.get_office_cell(OUT ref_cursor refcursor)
 RETURNS refcursor
 LANGUAGE plpgsql
AS $function$
declare

begin

    open ref_cursor for
select
  o.id,
  o.mainkey,
  o.createdate,
  o.fullname,
  o.jobtitle,
  o.phone,
  o.responsiblefor,
  o.titleorder,
  o.parentkey,
  o.typerecord
from
  user_directory.officecell o;
end;
$function$
;



-- ** ----- Функции удаления  ** ------

-- DROP FUNCTION user_directory.delete_trader_cell(int4);

CREATE OR REPLACE FUNCTION user_directory.delete_trader_cell(p_id integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
BEGIN
    DELETE FROM user_directory.tradercell
    WHERE id = p_id;

    IF NOT FOUND THEN
        RETURN 'error';
    ELSE
        RETURN 'ok';
    END IF;
END;
$function$
;

-- DROP FUNCTION user_directory.delete_portativ_cell(int4);

CREATE OR REPLACE FUNCTION user_directory.delete_portativ_cell(p_id integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
BEGIN
    DELETE FROM user_directory.portativcell
    WHERE id = p_id;

    IF NOT FOUND THEN
        RETURN 'error';
    ELSE
        RETURN 'ok';
    END IF;
END;
$function$
;

-- DROP FUNCTION user_directory.delete_partner_cell(int4);

CREATE OR REPLACE FUNCTION user_directory.delete_partner_cell(p_id integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
BEGIN
    DELETE FROM user_directory.partnerscell
    WHERE id = p_id;

    IF NOT FOUND THEN
        RETURN 'error';
    ELSE
        RETURN 'ok';
    END IF;
END;
$function$
;

-- DROP FUNCTION user_directory.delete_office_cell(int4);

CREATE OR REPLACE FUNCTION user_directory.delete_office_cell(p_id integer)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
BEGIN
    DELETE FROM user_directory.officecell
    WHERE id = p_id;

    IF NOT FOUND THEN
        RETURN 'error';
    ELSE
        RETURN 'ok';
    END IF;
END;
$function$
;



-- ** ----- DDL создание таблиц  ** ------


-- user_directory.officecell определение

-- Drop table

-- DROP TABLE user_directory.officecell;

CREATE TABLE user_directory.officecell (
	id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
	mainkey varchar NULL,
	createdate varchar NULL,
	fullname varchar NULL,
	jobtitle varchar NULL,
	phone varchar NULL,
	responsiblefor varchar NULL,
	titleorder int8 NULL,
	parentkey varchar NULL,
	typerecord varchar NULL,
	firedstatus int8 NULL,
	CONSTRAINT officecell_pkey PRIMARY KEY (id)
);



-- user_directory.partnerscell определение

-- Drop table

-- DROP TABLE user_directory.partnerscell;

CREATE TABLE user_directory.partnerscell (
	id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
	createdate varchar NULL,
	titleorder int8 NULL,
	servicename varchar NULL,
	bankname varchar NULL,
	fullname varchar NULL,
	phone varchar NULL,
	description varchar NULL,
	CONSTRAINT partnerscell_pkey PRIMARY KEY (id)
);


-- user_directory.portativcell определение

-- Drop table

-- DROP TABLE user_directory.portativcell;

CREATE TABLE user_directory.portativcell (
	id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
	mainkey varchar NULL,
	createdate varchar NULL,
	titleorder int8 NULL,
	codeto varchar NULL,
	city varchar NULL,
	address varchar NULL,
	place varchar NULL,
	mobileoperator varchar NULL,
	schedule varchar NULL,
	phone varchar NULL,
	headofsector varchar NULL,
	CONSTRAINT portativcell_pkey PRIMARY KEY (id)
);


-- user_directory.tradercell определение

-- Drop table

-- DROP TABLE user_directory.tradercell;

CREATE TABLE user_directory.tradercell (
	id int8 GENERATED ALWAYS AS IDENTITY( INCREMENT BY 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1 NO CYCLE) NOT NULL,
	mainkey varchar NULL,
	createdate varchar NULL,
	titleorder int8 NULL,
	codeto varchar NULL,
	city varchar NULL,
	address varchar NULL,
	place varchar NULL,
	mobileoperator varchar NULL,
	schedule varchar NULL,
	phone varchar NULL,
	headofsector varchar NULL,
	CONSTRAINT tradercell_pkey PRIMARY KEY (id)
);


-- ** ----- селекты  ** ------


SELECT id, mainkey, createdate, fullname, jobtitle, phone, responsiblefor, titleorder, parentkey, typerecord, firedstatus 
FROM user_directory.officecell;

SELECT id, mainkey, createdate, titleorder, codeto, city, address, place, mobileoperator, schedule, phone, headofsector
FROM user_directory.tradercell;

SELECT id, createdate, titleorder, servicename, bankname, fullname, phone, description
FROM user_directory.partnerscell;

SELECT id, mainkey, createdate, titleorder, codeto, city, address, place, mobileoperator, schedule, phone, headofsector
FROM user_directory.portativcell;